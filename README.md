app2 adalah sebuah service sederhana yang menangani master user. saya step-by-step mengikuti tutorial dari sumber yang telah saya cantumkan di bawah. namun ada beberapa perubahan dan penyempurnaan yang saya lakukan.

Ada beberapa field yang perlu diketahui:
1. userid
- Id dari sebuah pengguna.
- Integer.

2. name
- Nama dari sebuah pengguna.
- String.

3. username
- Username dari sebuah pengguna.
- String.

4. email
- Email dari sebuah pengguna.
- String.

Dengan menggunakan postman, ada 4 REST API di sini:
1. GET
- Untuk melihat sebuah data pengguna.
- userid wajib diisi
- api: localhost:5001/user/{userid} -> Method GET
- contoh: localhost:5001/user/1
- hasil: { "id": 1, "name": "Bejo", "username": "bejobejo", "email": "bejo@bejo" }


2. PUT
- Untuk menambah sebuah data pengguna.
- Seluruh data wajib diisi
- api: localhost:5001/user/{userid}/ -> Method PUT
- link: localhost:5001/user/{userid}?name={username}&username={userusername}&email={useremail}
- contoh: localhost:5001/user/4?name=Bakmi&username=bakmi&email=bakmi@bakmi.com
- hasil: { "id": 4, "name": "Bakmi", "username": "bakmi", "email": "bakmi@bakmi.com" }
- id tidak bisa duplikat

3. PATCH
- Untuk memperbarui sebuah data pengguna.
- Seluruh data opsional diisi, kecuali userid
- api: localhost:5001/user/{userid}/ -> Method PATCH
- link: localhost:5001/user/{userid}?name={username}&username={userusername}&email={useremail}
- contoh: localhost:5001/user/4?name=Bakmi Ayam&username=bakmiayam&email=bakmi@ayam.com
- hasil: { "id": 4, "name": "Bakmi", "username": "bakmiayam", "email": "bakmi@ayam.com" }

4. DELETE
- Untuk menghapus sebuah data pengguna.
- api: localhost:5001/user/{userid} -> Method DELETE
- contoh: localhost:5001/user/1
- hasil: { "id": 1, "name": "Bejo", "username": "bejobejo", "email": "bejo@bejo.com" }
- pada run pertama, hasil akan menunjukkan datanya, lalu jalankan lagi, menunjukkan pengguna tidak ada.

Sumber  :
https://www.youtube.com/watch?v=GMppyAPbLYk&ab_channel=TechWithTim