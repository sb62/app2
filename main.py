from flask import Flask, request
from flask_restful import Api, Resource, reqparse, abort, fields, marshal_with
from flask_sqlalchemy import SQLAlchemy, Model

app = Flask(__name__)
api = Api(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database2.db'
db = SQLAlchemy((app))
db.create_all()


class UserModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    username = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(100), nullable=False)

    def __repr__(self):
        return f"User(name={name}, username={username}, email={email})"


# if no db
# db.create_all()


user_put_args = reqparse.RequestParser()
user_put_args.add_argument(
    "name", type=str, help="Name of the user is required", required=True)
user_put_args.add_argument(
    "username", type=str, help="Username of the user is required", required=True)
user_put_args.add_argument(
    "email", type=str, help="Email of the user is required", required=True)

user_patch_args = reqparse.RequestParser()
user_patch_args.add_argument(
    "name", type=str, help="Name of the user is required")
user_patch_args.add_argument(
    "username", type=str, help="Username of the user is required")
user_patch_args.add_argument(
    "email", type=str, help="Email of the user is required")

resource_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'username': fields.String,
    'email': fields.String
}


class Users(Resource):
    @marshal_with(resource_fields)
    def get(self, user_id):
        result = UserModel.query.filter_by(id=user_id).first()
        if not result:
            abort(404, message="User not exists")
        return result

    @marshal_with(resource_fields)
    def put(self, user_id):
        args = user_put_args.parse_args()
        result = UserModel.query.filter_by(id=user_id).first()
        if result:
            abort(409, message="User id already exists")
        user = UserModel(
            id=user_id, name=args['name'], username=args['username'], email=args['email'])
        db.session.add(user)
        db.session.commit()
        return user, 201

    @marshal_with(resource_fields)
    def patch(self, user_id):
        args = user_patch_args.parse_args()
        result = UserModel.query.filter_by(id=user_id).first()
        if not result:
            abort(404, message="User not exists")

        if args['name']:
            result.name = args['name']
        if args['username']:
            result.username = args['username']
        if args['email']:
            result.email = args['email']

        # db.session.commit()
        return result

        # db.session.update(result)
        # db.session.commit()
        return result

    @marshal_with(resource_fields)
    def delete(self, user_id):
        result = UserModel.query.filter_by(id=user_id).first()
        if not result:
            abort(404, message="User not exists")
        db.session.delete(result)
        db.session.commit()
        return result


api.add_resource(Users, "/user/<int:user_id>")

if __name__ == "__main__":
    app.run(debug=True, port=5001)
